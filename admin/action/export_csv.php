<?php
session_start();
require '../includes/config.php';

if ($_SESSION['signin_check']) {

  header('Content-Type: application/csv');
  header('Content-Disposition: attachment; filename="participants.csv";');

  $file = fopen('php://output', 'w');


  $sql = "SELECT * FROM participants WHERE Deleted=0";
  $participants = mysqli_query($conn, $sql);

  while ($column = mysqli_fetch_field($participants)) {
    $columns[] = $column->name;
  }

  fputcsv($file, $columns);

  foreach ($participants as $line) {
    fputcsv($file, $line);
  }

} else {
  $_SESSION['signin_check'] = 0;
  header("Location: $baseurl");
}

?>
