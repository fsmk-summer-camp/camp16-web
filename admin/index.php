<?php
  session_start();
  require 'includes/config.php';
  require 'includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
  $username = $_SESSION['username'];
  $name = $_SESSION['name'];
?>

<body>
  <?php
    require 'includes/nav.php';
  ?>
  <div class="container">

    <h1>Hi<?php if($name != '') { echo ', '. $name; } ?>!</h1>
    <h2>Profile</h2>
    <form name="profile" action="action/profile.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="username" value="<?=$username ?>" readonly>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="name" value="<?=$name ?>" placeholder="Enter your name" required>
      </div>
      <div class="form-group">
        <input class="form-control" type="password" name="password" placeholder="Enter new password">
      </div>
      <div class="form-group">
        <input class="form-control" type="password" name="cpassword" placeholder="Confirm new password">
      </div>
      <div class="form-group">
        <input type="submit" value="Update" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<?php
  require 'includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
