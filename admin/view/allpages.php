<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $sql = "SELECT * FROM pages";
    $pages = mysqli_query($conn, $sql);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>All Pages</h2>
    <a href="<?=$baseurl ?>/view/newpage.php" class="btn btn-success">Add Page</a>

    <table class="table">
      <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Edit</th>
        <th>Delete</th>
      </thead>
      <tbody>
        <?php
          while ($page = mysqli_fetch_object($pages)) {
        ?>
        <tr>
          <td><?=$page->ID ?></td>
          <td><?=$page->Title ?></td>
          <td><a href="<?=$baseurl ?>/view/page.php?id=<?=$page->ID ?>">Edit</a></td>
          <td><a href="<?=$baseurl ?>/action/deletepage.php?id=<?=$page->ID ?>">Delete</a></td>
        </tr>
        <?php
      }
        ?>
    </table>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
