<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $sql = "SELECT * FROM users";
    $users = mysqli_query($conn, $sql);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>All Volunteers</h2>
    <a href="<?=$baseurl ?>/view/newuser.php" class="btn btn-success">Add User</a>
    <table class="table">
      <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Username</th>
      </thead>
      <tbody>
        <?php
          while ($user = mysqli_fetch_object($users)) {
        ?>
        <tr>
          <td><?=$user->ID ?></td>
          <td><?=$user->Name ?></td>
          <td><?=$user->Username ?></td>
        </tr>
        <?php
      }
        ?>
    </table>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
