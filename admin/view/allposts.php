<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $sql = "SELECT * FROM posts";
    $posts = mysqli_query($conn, $sql);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>All Posts</h2>
    <a href="<?=$baseurl ?>/view/newpost.php" class="btn btn-success">Add Post</a>
    <table class="table">
      <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Date</th>
        <th>Edit</th>
        <th>Delete</th>
      </thead>
      <tbody>
        <?php
          while ($post = mysqli_fetch_object($posts)) {
        ?>
        <tr>
          <td><?=$post->ID ?></td>
          <td><?=$post->Title ?></td>
          <td><?=$post->Date ?></td>
          <td><a href="<?=$baseurl ?>/view/post.php?id=<?=$post->ID ?>">Edit</a></td>
          <td><a href="<?=$baseurl ?>/action/deletepost.php?id=<?=$post->ID ?>">Delete</a></td>
        </tr>
        <?php
      }
        ?>
    </table>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
