<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>New Volunteer</h2>
    <form name="newvolunteer" action="../action/newvolunteer.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="name" id="name" placeholder="Name" autofocus required>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="picture" id="picture" placeholder="Picture" readonly>
      </div>
      <div class="form-group">
        <input class="form-control" type="email" name="email" placeholder="Email" required>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="phone" placeholder="Phone" required>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="college" placeholder="College" required>
      </div>
      <div class="form-group">
        <select name="track" class="form-control">
          <option value="Web">Web</option>
          <option value="Animation">Animation</option>
          <option value="Hardware">Hardware</option>
        </select>
      </div>
      <div class="form-group">
        <input type="submit" value="Add Volunteer" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<script type="text/javascript">
var t1 = document.getElementById('name');
var t2 = document.getElementById('picture');

t1.addEventListener('keyup', function () { t2.value = t1.value.replace(/[^\w]+/g, '-'); }, true);

</script>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
