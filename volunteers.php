<?php
session_start();
require 'includes/config.php';
?>
<?php
require 'includes/head.php';
?>
<?php
require 'includes/nav.php';
?>
<body>
  <?php
  $sql = "SELECT * FROM Volunteers ORDER BY Track";
  $people = mysqli_query($conn, $sql);
  ?>
  <div class="container">
    <div class="content list">
      <h1 class="page-title">Volunteers<div class="page-title-stop">.</div></h1>
      <div class="volunteers-wrapper">
      <?php
      while ($person = mysqli_fetch_object($people)) {
        $picture = strtoupper($person->Picture);
        ?>
        <div class="contact-box">
          <img src="<?=$baseurl ?>/images/volunteers/SHUBODIP-HOSH.jpg">
          <?=$person->Name ?>
          <br>
          <?=$person->Track ?>
        </div>
        <?php
      }
      ?>
    </div>
    </div>
  </div>
  <?php
  require 'includes/foot.php';
  ?>
