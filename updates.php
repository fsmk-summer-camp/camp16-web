<?php
session_start();
require 'includes/config.php';
?>
<?php
require 'includes/head.php';
?>
<body>
  <?php
  require 'includes/nav.php';
  ?>
  <?php
  $sql = "SELECT * FROM posts ORDER BY Date DESC";
  $posts = mysqli_query($conn, $sql);
  ?>
  <div class="container">
    <div class="content list">
      <h1 class="page-title">Updates<div class="page-title-stop">.</div></h1>
      <?php
      while ($post = mysqli_fetch_object($posts)) {
        ?>
        <div class="list-item list-item-updates">
          <h2 class="list-post-title">
            <a href="<?=$baseurl ?>/post/<?=$post->Slug ?>"><?=$post->Title ?></a>
          </h2>
          <div class="list-post-date">
            <time><?php

            $date = DateTime::createFromFormat('Y-m-d H:i:s', $post->Date)->format('d M Y');
            $time = DateTime::createFromFormat('Y-m-d H:i:s', $post->Date)->format('H:i');
            echo 'Posted on ' . $date . ' at ' . $time;

            ?></time>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
  <?php
  require 'includes/foot.php';
  ?>
